import { createMessage } from "../services/messageService";

const sendMessage = async (req,res) => {
    sender = req.user.email;
    receiver = req.body.email;
    message_content = req.body.messageContent;

    const message = await createMessage({sender,receiver,message_content})
    
    return res.status(200).json(message);
}

export {sendMessage};