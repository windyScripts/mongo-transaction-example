// mongoose for running transactions

import mongoose from 'mongoose';

import { registerUser, findAllUsers, findUser, deleteUser, saveUser } from "../services/userServices"
import { createRequest, findRequest,createRequest, saveRequest } from "../services/requestService";
import { findMessages, deleteMessages } from "../services/messageService";

// register creates a new user if the email is new

const register = async (req,res) => {
    const emailIsUsed = !(await findUser({email:req.body.email}))
    if(emailIsUsed){
        return res.status(400).json({message:"That email is already taken."})
    }

    try{
const {email, password} = req.body;
const user = await registerUser({email,password});
return res.status(200).json(user);
}catch(err){
    res.status(500).json(err);
}
}

const getUser = async (req,res) => {
    try{
const {email} = req.body;
const user = await findUser({email})
return res.status(200).json(user);
}catch(err){
    res.status(500).json(err)
}
}

const getUsers = async (req,res) => {
try{
    const users = await findAllUsers();
    return res.status(200).json(users);
}catch(err){
    res.status(500).json(err);
}
}

const sendFriendRequest = async (req,res) => {

const sender = req.user.email;
const receiver = req.body.email;

const senderObject = await findUser({email:sender});
const receiverObject = await findUser({email:receiver})

const pastRequest = await findRequest({sender,receiver,active:true})


if(pastRequest) {

    const session = await mongoose.startSession();

    session.startTransaction();

    try{
    pastRequest.active = false;
    await saveRequest(pastRequest);
    senderObject.sent_requests = senderObject.sent_requests.filter(e => e.requestId !== pastRequest.requestId);
    receiverObject.received_requests = receiverObject.received_requests.filter(e => e.requestId !== pastRequest.requestId);
    await saveUser(senderObject);
    await saveUser(receiverObject);

    await session.commitTransaction();

    session.endSession();
}catch(err){
    session.abortTransaction();
    session.endSession();
}
}

const newRequest = await createRequest({sender,receiver});
senderObject.sent_requests.push()
receiverObject.received_requests.push(newRequest)

const session = await mongoose.startSession();

session.startTransaction();

try{

await saveUser(senderObject);
await saveUser(receiverObject);

await session.commitTransaction();

session.endSession();
res.status(200).json(newRequest);
}catch(err){
    session.abortTransaction();
    session.endSession();
    res.status(500)
}

}

const login = async (req,res) => {

}



const acceptFriendRequest = async (req,res) => {
    let request;
    try{
const receiver = req.user.email;
const sender = req.body.email;
request = await findRequest({sender,receiver,active:true})
    }catch(err){
        res.status(404).json(err);
    }
if(request){


    const session = await mongoose.startSession();

    session.startTransaction();

    try{

    const senderObject = await findUser({email:sender});
    const receiverObject = await findUser({email:receiver});

    // transaction here

    senderObject.friends.push(receiver);
    receiverObject.friends.push(sender);
    senderObject.sent_requests = senderObject.sent_requests.filter(e => e.requestId !== request.requestId)
    receiverObject.received_requests = receiverObject.received_requests.filter(e => e.requestId !== request.requestId)
    request.active = false;

    saveRequest(request);
    saveUser(senderObject);
    saveUser(receiverObject);

await session.commitTransaction();

session.endSession();
    }catch(err){
        await session.abortTransaction();
        session.endSession();
    }

}
}

const deleteUserAccount = async (req,res) => {
    const email = req.user.email;
    const removeUser = deleteUser({email})
    const removeMessages = deleteMessages({sender:email})

// create session and transaction
    await session.commitTransaction();

    session.endSession();

    try{
    await Promise.all([removeMessages,removeUser])

    await session.commitTransaction();
    
    session.endSession();
}catch(err){
    await session.abortTransaction();
    session.endSession();
}
}

export {login,sendFriendRequest, acceptFriendRequest,getUser,register, getUsers, deleteUserAccount}