import Router from 'express';
const router = Router();

import { login,friendUser,getUser, getUsers, register } from "../controllers/user";

router.post('/new',register)

router.get('/',getUsers);

router.put('/add',friendUser);

export default router;