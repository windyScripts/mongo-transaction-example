import Request from '../models/request'

export const createRequest = (params) => {
    return Request.create(params)
} 

export const findRequest = (params) => {
    return Request.findOne(params)
}

export const saveRequest = (request) => {
    return request.save();
}