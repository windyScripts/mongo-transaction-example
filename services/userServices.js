import User from '../models/user.js';

export const registerUser = (params) => {
    return User.create(params)
}

export const findAllUsers = (params) => {
    return User.find(params);
}

export const findUser = (params) => {
    return User.findOne(params)
}

export const saveUser = (user) => {
return user.save();
}

export const deleteUser = (params) => {
User.deleteOne(params);
}
