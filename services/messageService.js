import Message from '../models/message'

export const createMessage = (params) => {
    return Message.create(params)
}

export const deleteMessages = (params) =>{
    return Message.deleteMany(params);
}

export const findMessages = (params) => {
    return Message.find(params)
}