#!/bin/bash
# Wait for few seconds until the mongo server is up
sleep 5
echo "Setting up mongo replica set"
mongo --host mongo1:30001 <<EOF
  var cfg = {
    "_id": "my-replica-set",
    "members": [
      {
        "_id": 0,
        "host": "mongo1:30001"
      },
      {
        "_id": 1,
        "host": "mongo2:30002"
      },
      {
        "_id": 2,
        "host": "mongo3:30003"
      }
    ]
  };
  rs.initiate(cfg);
  rs.status();
EOF