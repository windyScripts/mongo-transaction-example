import app from 'express';
import mongoose from 'mongoose'
import dotenv from "dotenv";
import cors from 'cors';

// import routes
import friendRoutes from './routes/user.js';
import messageRoutes from './routes/message.js'

dotenv.config();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// app.use('/',routes)
app.use('/',friendRoutes);
app.use('/messages',messageRoutes)

app.use(cors({
  origin: '*',
  methods: ['GET', 'POST', 'PUT'],
}));

app.use((req, res) => {
  res.sendFile(path.join(__dirname, 'public', req.url));
});

async function connectDB() {
    try{
    await mongoose.connect(process.env.DATABASE_URL);
    console.log('Database connected. :)');
    }
    catch (error) {
      console.log(error);
      process.exit(1);
    }
  }
  
  connectDB().then(()=>{
    app.listen(process.env.PORT || 3000);
    console.log("listening for requests!")
  });