import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
    unique: true
  },
  date: { type: Date, default: Date.now },
  friends: {type: Array, default: []},
  sent_requests: {type: Array, default: []},
  received_requests: {type: Array, default: []}
});

export default new mongoose.model('User', userSchema);