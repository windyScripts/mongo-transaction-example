import mongoose from 'mongoose';
import User from './user';

const Schema = mongoose.Schema;

const requestSchema = new Schema({
sender: {
    type:User, 
    required: true},
receiver: {
    type:User,
    required: true
},
requestId: {
    type:String,
    unique:true,
    required:true
},
active:{
    type: Boolean,
    default: true
},
date: { type: Date, default: Date.now }
});


export default new mongoose.model('Request', requestSchema);