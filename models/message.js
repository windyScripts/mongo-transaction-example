import mongoose from 'mongoose';
import User from './user';

const Schema = mongoose.Schema;

const messageSchema = new Schema({
sender: {
    type:User, 
    required: true},
receiver: {
    type:User,
    required: true
},
messageId: {
    type:String,
    unique:true,
    required:true
},
message_content:{
    type:String
},
date: { type: Date, default: Date.now }
});


export default new mongoose.model('Request', messageSchema);